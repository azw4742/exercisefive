package com.example.andrew.exercisefive;

/**
 * Created by Andrew on 11/6/2015.
 */
public class Movie
{
    String title;
    String year;
    String director;


    public Movie()
    {
        this.title = title;
        this.year = year;
        this.director = director;
    }

    public Movie(String title, String year, String director)
    {
        this.title = title;
        this.year = year;
        this.director = director;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getDirector() {
        return director;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setDirector(String director) {
        this.director = director;
    }

}
