package com.example.andrew.exercisefive;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class exerciseFive extends ListActivity{

    private ListView list;
    private TreeMap<String, String> displayMap;
    private TreeMap<String, String> valueMap;
    private ArrayList<Movie> movieList = new ArrayList<Movie>();
    private String[] abc = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N",
            "O","P","Q","R","S","T","U","V","W","X","Y","Z"};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_five);
        iniList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exercise_five, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void iniList()
    {

        displayMap = new TreeMap<String, String>();
        valueMap = new TreeMap<String, String>();
        for (int i = 0; i < abc.length; i++)
        {
            displayMap.put(abc[i], "");
            valueMap.put(abc[i], "");
        }
        MyAdapter adapter = new MyAdapter(displayMap);
        list = (ListView) findViewById(android.R.id.list);
        list.setAdapter(adapter);
    }

    public void testDialog()
    {
        String keys = "";
        for ( String key : displayMap.keySet()) {
            keys = keys + key + ",";
        }
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Reset...");
        alertDialog.setMessage(keys);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
// here you can add functions
            }
        });
        alertDialog.show();
    }

    public void testClick(View v)
    {
        testDialog();
    }

    public void addMovieDialog(View v)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Movie");
        builder.setCancelable(true);
        LinearLayout lila1= new LinearLayout(this);
        lila1.setOrientation(LinearLayout.VERTICAL); //1 is for vertical orientation
        final EditText titleInput = new EditText(this);
        final EditText yearInput = new EditText(this);
        final EditText dirInput = new EditText(this);
        final TextView title = new TextView(this);
        final TextView year = new TextView(this);
        final TextView director = new TextView(this);

        title.setText(getString(R.string.add_title));
        year.setText(getString(R.string.add_year));
        director.setText(getString(R.string.add_dir));

        lila1.addView(title);
        lila1.addView(titleInput);
        lila1.addView(year);
        lila1.addView(yearInput);
        lila1.addView(director);
        lila1.addView(dirInput);

        builder.setView(lila1);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String title = titleInput.getText().toString();
                String year = yearInput.getText().toString();
                String director = dirInput.getText().toString();
                validateAddMovie(title,year,director);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void validateAddMovie(String title, String year, String dir)
    {
        if(title.equals(null) || title.equals("") || year.equals(null) || year.equals("") || dir.equals(null) || dir.equals(""))
        {
            failAddMovie();
        }
        else
        {
            passAddMovie(title, year,dir);
        }
    }

    public void failAddMovie()
    {
        final View v = this.getCurrentFocus();
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Fail to add!");
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setMessage("Please add values when adding or updating a new movie");
        alertDialog.setButton("Try again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
                addMovieDialog(v);
            }
        });
        alertDialog.show();
    }

    public void passAddMovie(String title, String year, String dir)
    {
        Movie movie = new Movie(title,year,dir);
        movieList.add(movie);
        addValues(movie);
    }

    public void addValues(Movie movie)
    {
        int i = 0;
        int z = 0;
        for (int y = 0; y < abc.length; y++)
        {
            String keyMatch = movie.getTitle().substring(0, 1);
            if (keyMatch.equalsIgnoreCase(abc[y]))
            {
                String newValue = valueMap.get(abc[y]);
                newValue = newValue + "\n" + "Title: " + movie.getTitle() + "\n" + "Year: "  + movie.getYear() + "\n" + "Director: " + movie.getDirector() + "\n";
                valueMap.put(abc[y], newValue);
            }
        }
        regenListView();
    }

    public void regenListView()
    {
        for(int i=0; i < displayMap.size(); i++)
        {
            displayMap.put(abc[i], valueMap.get(abc[i]));
        }
        MyAdapter adapter = new MyAdapter(displayMap);
        list = (ListView) findViewById(android.R.id.list);
        list.setAdapter(adapter);
    }

    public void updateValues()
    {
        clearValues();
        for(int i=0; i < movieList.size(); i++)
        {
            Movie movie = movieList.get(i);
            for(int y=0; y < abc.length; y++)
            {
                String keyMatch = movie.getTitle().substring(0, 1);
                if(keyMatch.equalsIgnoreCase(abc[y]))
                {
                    String newValue = valueMap.get(abc[y]);
                    newValue = newValue + "\n" + "Title: " + movie.getTitle() + "\n" + "Year: "  + movie.getYear() + "\n" + "Director: " + movie.getDirector() + "\n";
                    valueMap.put(abc[y],newValue);
                }
            }
        }
        regenListView();
    }

    public void updateMovieDialog(View v)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Update Movie");
        builder.setCancelable(true);
        LinearLayout lila1= new LinearLayout(this);
        lila1.setOrientation(LinearLayout.VERTICAL); //1 is for vertical orientation
        final EditText titleInput = new EditText(this);
        final EditText yearInput = new EditText(this);
        final EditText dirInput = new EditText(this);
        final TextView title = new TextView(this);
        final TextView year = new TextView(this);
        final TextView director = new TextView(this);
        final TextView message = new TextView(this);

        message.setText("Enter movie title and new information to update. To update movie title, delete the title in question first.");
        title.setText(getString(R.string.add_title));
        year.setText(getString(R.string.add_year));
        director.setText(getString(R.string.add_dir));

        lila1.addView(message);
        lila1.addView(title);
        lila1.addView(titleInput);
        lila1.addView(year);
        lila1.addView(yearInput);
        lila1.addView(director);
        lila1.addView(dirInput);

        builder.setView(lila1);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String title = titleInput.getText().toString();
                String year = yearInput.getText().toString();
                String director = dirInput.getText().toString();
                validateUpdateMovie(title, year, director);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void validateUpdateMovie(String title, String year, String dir)
    {
        if(title.equals(null) || title.equals("") || year.equals(null) || year.equals("") || dir.equals(null) || dir.equals(""))
        {
            failUpdateMovie();
        }
        else
        {
            passUpdateMovie(title, year, dir);
        }
    }

   public void passUpdateMovie(String title, String year, String dir)
   {
       boolean passFail = false;
       for(int i=0; i < movieList.size(); i++)
       {
           if(title.equals(movieList.get(i).getTitle()))
           {
               movieList.remove(i);
               Movie movie = new Movie(title,year,dir);
               movieList.add(movie);
               passFail = true;
           }
       }
       if(passFail == true)
       {
           updateValues();
       }
       else {
           noMovieTitle();
       }
   }

    public void clearValues()
    {
        for(int i=0; i < abc.length; i++)
        {
            valueMap.put(abc[i],"");
        }
    }

    public void failUpdateMovie()
    {
        final View v = this.getCurrentFocus();
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Fail to update!");
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setMessage("Please enter an existing movie title with real data!");
        alertDialog.setButton("Try again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
                updateMovieDialog(v);
            }
        });
        alertDialog.show();
    }

    public void noMovieTitle()
    {
        final View v = this.getCurrentFocus();
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Fail to update!");
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setMessage("Please enter an existing movie title!");
        alertDialog.setButton("Try again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
                updateMovieDialog(v);
            }
        });
        alertDialog.show();
    }

    public void deleteMovieDialog(View v)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Movie");
        builder.setCancelable(true);
        LinearLayout lila1= new LinearLayout(this);
        lila1.setOrientation(LinearLayout.VERTICAL); //1 is for vertical orientation
        final EditText titleInput = new EditText(this);
        final EditText yearInput = new EditText(this);
        final EditText dirInput = new EditText(this);
        final TextView title = new TextView(this);
        final TextView year = new TextView(this);
        final TextView director = new TextView(this);
        final TextView message = new TextView(this);

        message.setText("Enter movie title to be deleted.");
        title.setText(getString(R.string.add_title));

        lila1.addView(message);
        lila1.addView(title);
        lila1.addView(titleInput);

        builder.setView(lila1);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String titleText = titleInput.getText().toString();
                validateDeleteMovie(titleText);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void validateDeleteMovie(String title)
    {
        boolean passFail = false;
        for(int i=0; i < movieList.size(); i++)
        {
            if(title.equals(movieList.get(i).getTitle()))
            {
                movieList.remove(i);
                updateValues();
                passFail = true;
            }
        }
        if(passFail == true)
        {
            movieDeletedDialog();
        }
        else
        {
            movieDeleteFail();
        }
    }

    public void movieDeletedDialog()
    {
        final View v = this.getCurrentFocus();
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Movie deleted!");
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setMessage("Movie deleted!");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void movieDeleteFail()
    {
        final View v = this.getCurrentFocus();
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Movie deletion failure!");
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setMessage("Verify existing movie title and try again");
        alertDialog.setButton("Try again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
                deleteMovieDialog(v);
            }
        });
        alertDialog.show();
    }


}
